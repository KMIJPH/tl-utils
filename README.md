# tl-utils

## Description
A growing collection of teal/lua functions needed for various things.  
Functions should be (mostly) documented.  
Can be compiled to a lua module.

Type definitions can be found [here](src/tl-utils.d.tl).

## Installation
```bash
tl build
luarocks make tl-utils-<version>.rockspec --local  # install compiled lua module
```
