-- File Name: str.tl
-- Description: String functions
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 23 May 2023 20:46:21
-- Last Modified: 04 Sep 2024 12:47:14

local str <const> = {}

--- returns line count of string
function str.line_count(s: string): integer
    local count = 1
    for i = 1, #s do
        local c <const> = s:sub(i, i)
        if c == '\n' then count = count + 1 end
    end
    return count
end

--- splits string by newlines (without deleting empty lines)
function str.split_lines(s: string): {string}
    local lines: {string} = {}
    local current_line: {string} = {}
    local inside_quotes: boolean = false
    local i: integer = 1

    while i <= #s do
        local char = s:sub(i, i)

        -- Handle quotes to avoid splitting inside strings
        if char == '"' or char == "'" then
            inside_quotes = not inside_quotes
            table.insert(current_line, char)
        elseif char == "\\" and s:sub(i + 1, i + 1) == "n" and inside_quotes then
            -- Handle escaped \n
            table.insert(current_line, "\\n")
            i = i + 1
        elseif char == "\n" and not inside_quotes then
            -- Split by real newline
            table.insert(lines, table.concat(current_line))
            current_line = {}
        elseif char == "\n" and inside_quotes then
            table.insert(current_line, "\\n")
        else
            table.insert(current_line, char)
        end
        i = i + 1
    end

    if #current_line > 0 then
        table.insert(lines, table.concat(current_line))
    end

    return lines
end

--- splits string by separator
function str.split(s: string, sep: string): {string}
    local pieces: {string} = {}
    for piece in s:gmatch(("([^%s]+)"):format(sep)) do
        table.insert(pieces, piece)
    end

    return pieces
end

--- trims whitespaces from start and end
function str.trim(s: string): string
    local r <const> = s:gsub("[%s]+$", ""):gsub("^[%s]+", "")
    return r
end

--- escapes every character (useful for regexes.. regexi?)
function str.regex_escape(s: string): string
    local escaped = s:gsub("[^%w]", "%%%1")
    return escaped
end

--- escapes 'char' in 's' with 'with'
function str.escape(s: string, char: string, with: string): string
    with = with or "\\"
    local r <const> = s:gsub(char, ("%s%s"):format(with, char))
    return r
end

--- checks if 's' starts with 's1'
function str.startswith(s: string, s1: string): boolean
    local m = s:match(("^%s.*"):format(s1))
    return m ~= nil
end


return str
