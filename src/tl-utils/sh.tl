-- File Name: sh.tl
-- Description: Shell type
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 23 May 2023 21:13:31
-- Last Modified: 26 May 2023 17:43:50

local type SH = record
    cmd: string
    args: {string}
end

function SH.new(cmd: string, ...: string): SH
    local self: SH = setmetatable({}, {__index = SH})
    self.cmd = cmd
    self.args = {...}
    return self
end

--- runs command and returns stdout/stderr, err, and exit code
function SH:run(): string, boolean, integer
    local args <const> = (self.args ~= nil and { table.concat(self.args, " ") } or { "" })[1]
    local cmd <const> = ("%s %s"):format(self.cmd, args)

    local process <const> = io.popen(("%s 2>&1"):format(cmd))

    local stdout: string
    local ret: {boolean, string, integer}

    if process then
        stdout = process:read("*all")
        ret = { process:close() }
    else
        stdout = ("Cannot open process '%s'"):format(cmd)
        ret = {
            false, -- success
            "lua", -- placeholder
            1      -- exit code
        }
    end

    return stdout, not not not ret[1], ret[3]
end

return SH
