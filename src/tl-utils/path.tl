-- File Name: path.tl
-- Description: Path functions
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 23 May 2023 21:00:09
-- Last Modified: 26 May 2023 17:43:45

local path <const> = {}

--- returns directory separator
function path.sep(): string
    return package.config:sub(1, 1)
end

--- joins paths together
function path.join(...: string): string
    local s <const> = path.sep()
    local args <const> = { ... }
    local p <const> = table.concat(args, s):gsub(("%s%s"):format(s, s), s)
    return p
end

--- similar to python's os.path.basename
function path.filename(p: string): string
    local name <const> = p:match("^.+[/\\](.+)$")
    return (name ~= "" and { name } or { nil })[1]
end

--- same as filename() but removes extension
function path.basename(p: string): string
    local name <const> = p:match("^.+[/\\](.+)%..*$")
    return (name ~= "" and { name } or { nil })[1]
end

--- returns extension
function path.extension(p: string): string
    local ext <const> = p:match("^.+%.(.+)$")
    return (ext ~= "" and { ext } or { nil })[1]
end

--- returns parent directory
function path.parent(p: string): string
    local par = p:match("^(.*)[//\\]")
    return (par ~= "" and { par } or { nil })[1]
end

--- returns the directory up until the path where subdir was found
function path.subdir(p: string, sub: string): string
    local pattern <const> = ("^(.+)/%s"):format(sub)
    local m <const> = string.match(p, pattern)
    return m
end

return path
