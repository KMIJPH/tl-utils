#!/usr/bin/env lua
--
-- File Name: str.lua
-- Description: String tests
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 22 May 2023 12:36:28
-- Last Modified: 13 Sep 2024 09:56:34

package.path = './src/tl-utils/?.tl;' .. package.path

local inspect = require("inspect")
local tl = require("tl")
tl.loader()

local str = require("str")

local function line_count()
    local result = str.line_count("one\ntwo\nthree")
    local should_be = 3
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
end

local function split_lines()
    local result = str.split_lines("one\ntwo\nthree\n")
    local should_be = { "one", "two", "three" }
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
end

local function split()
    local result = str.split("one,two,three,,", ",")
    local should_be = { "one", "two", "three" }
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
end

local function trim()
    local result = str.trim("    hello world   ")
    local should_be = "hello world"
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
end

local function regex_escape()
    local result = str.regex_escape("--")
    local should_be = "%-%-"
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
    result = str.regex_escape("*/")
    should_be = "%*%/"
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
end

local function escape()
    local result = str.escape("hello ' world", "'")
    local should_be = "hello \\' world"
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
    result = str.escape("hello a world", "a")
    should_be = "hello \\a world"
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
end

local function startswith()
    local result = str.startswith("hello world", "hello")
    local should_be = true
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
    result = str.startswith("hello world", "world")
    should_be = false
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
end

local function main()
    line_count()
    split_lines()
    split()
    trim()
    regex_escape()
    escape()
    startswith()
end

return main
