#!/usr/bin/env lua
--
-- File Name: path.lua
-- Description: Path tests
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 22 May 2023 12:36:28
-- Last Modified: 13 Sep 2024 09:56:31

package.path = './src/tl-utils/?.tl;' .. package.path

local inspect = require("inspect")
local tl = require("tl")
tl.loader()

local path = require("path")

local function join()
    local sep = path.sep()
    local result = path.join("home", "user", "test")
    local should_be = ("home%suser%stest"):format(sep, sep)
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
end

local function filename()
    local result = path.filename("/home/user/test.txt")
    local should_be = "test.txt"
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
    result = path.filename("/home/user/test/file")
    should_be = "file"
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
    local result2 = path.filename("")
    local should_be2 = nil
    assert(
        inspect(result2) == inspect(should_be2),
        ("expected: %s, got: %s"):format(inspect(should_be2), inspect(result2))
    )
end

local function basename()
    local result = path.basename("/home/user/test.txt")
    local should_be = "test"
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
    result = path.basename("/home/user/test.2.pdf")
    should_be = "test.2"
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
end

local function extension()
    local result = path.extension("/home/user/test.txt")
    local should_be = "txt"
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
    result = path.extension("/home/user/test.2.pdf")
    should_be = "pdf"
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
end

local function parent()
    local result = path.parent("/home/user/test.txt")
    local should_be = "/home/user"
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
    result = path.parent("/home/user/file/test")
    should_be = "/home/user/file"
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
    local result2 = path.parent("/home")
    local should_be2 = nil
    assert(
        inspect(result2) == inspect(should_be2),
        ("expected: %s, got: %s"):format(inspect(should_be2), inspect(result2))
    )
end

local function subdir()
    local result = path.subdir("/home/user/test/dir", "user")
    local should_be = "/home"
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
    local result2 = path.subdir("/home/user/test/dir", "folder")
    local should_be2 = nil
    assert(
        inspect(result2) == inspect(should_be2),
        ("expected: %s, got: %s"):format(inspect(should_be2), inspect(result2))
    )
end

local function main()
    join()
    filename()
    basename()
    extension()
    parent()
    subdir()
end

return main
