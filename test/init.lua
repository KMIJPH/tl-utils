#!/usr/bin/env lua
--
-- File Name: init.lua
-- Description: Tests
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 22 May 2023 11:44:38
-- Last Modified: 13 Sep 2024 10:00:42

local arr = require("test.arr")
local tbl = require("test.tbl")
local str = require("test.str")
local path = require("test.path")

local function main()
    arr()
    tbl()
    str()
    path()
    print("all tests passed!")
end

main()
