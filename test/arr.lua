#!/usr/bin/env lua
--
-- File Name: arr.lua
-- Description:
-- Dependencies:
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 23 May 2023 22:43:23
-- Last Modified: 13 Sep 2024 09:56:25

package.path = './src/tl-utils/?.tl;' .. package.path

local inspect = require("inspect")
local tl = require("tl")
tl.loader()

local arr = require("arr")

local function join()
    local result = arr.join({ 1, 2 }, { 3, 4 })
    local should_be = { 1, 2, 3, 4 }
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
end

local function unique()
    local result = arr.unique({ 1, 2, 2, 3, 3, 4 })
    local should_be = { 1, 2, 3, 4 }
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
end

local function rep()
    local result = arr.rep("2", 3)
    local should_be = { "2", "2", "2" }
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
end

local function filter()
    local result = arr.filter({ 1, 2, 3, 4 }, function(a) return a > 2 end)
    local should_be = { 3, 4 }
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
end

local function contains()
    local result = arr.contains({ 1, 2, 3, 4 }, 3)
    local should_be = true
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
    result = arr.contains({ "a", "b", "cd", "ef" }, "g")
    should_be = false
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
end

local function map()
    local result = arr.map({ 1, 2, 3, 4 }, function(a) return a + 1 end)
    local should_be = { 2, 3, 4, 5 }
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
    result = arr.map({ 1, 2, 3, 4 }, function(a) return a > 1 end)
    should_be = { false, true, true, true }
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
end

local function min()
    local result = arr.min({ 1, 2, 3, 4 })
    local should_be = 1
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
    result = arr.min({})
    should_be = nil
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
end

local function max()
    local result = arr.max({ 1, 2, 3, 4 })
    local should_be = 4
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
    result = arr.max({})
    should_be = nil
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
end

local function main()
    join()
    join()
    unique()
    rep()
    filter()
    contains()
    map()
    min()
    max()
end

return main
