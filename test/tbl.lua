#!/usr/bin/env lua
--
-- File Name: tbl.lua
-- Description: Table functions tests
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 22 May 2023 06:56:36
-- Last Modified: 13 Sep 2024 09:56:38

package.path = './src/tl-utils/?.tl;' .. package.path

local inspect = require("inspect")
local tl = require("tl")
tl.loader()

local tbl = require("tbl")

local function join()
    local result = tbl.join({ a = 1, b = 2 }, { c = 3, d = 4 })
    local should_be = { a = 1, b = 2, c = 3, d = 4 }
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
end

local function extend()
    local result = tbl.extend({ a = 1, b = 2 }, { a = 3, d = 4 })
    local should_be = { a = 1, b = 2, d = 4 }
    assert(
        inspect(result) == inspect(should_be),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
end

local function keys()
    local result = tbl.keys({ a = 1, b = 2, d = 4 })
    local should_be = { "a", "b", "d" }
    assert(
        inspect(table.sort(result) == inspect(table.sort(should_be))),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
    result = tbl.keys({ [1] = "a", [2] = "3", [4] = "6" })
    should_be = { 1, 2, 4 }
    assert(
        inspect(table.sort(result) == inspect(table.sort(should_be))),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
end

local function values()
    local result = tbl.values({ a = 1, b = 2, d = 4 })
    local should_be = { 1, 2, 4 }
    assert(
        inspect(table.sort(result) == inspect(table.sort(should_be))),
        ("expected: %s, got: %s"):format(inspect(should_be), inspect(result))
    )
end

local function main()
    join()
    extend()
    keys()
    values()
end

return main
