package = "tl-utils"
version = "0.2-1"
rockspec_format = "3.0"
source = {
    url = "https://codeberg.org/KMIJPH/tl-utils"
}
description = {
    homepage = "https://codeberg.org/KMIJPH/tl-utils",
    license = "GPL3",
}
dependencies = {
    "tl",
}
build = {
    type = "builtin",
    modules = {
        ["tl-utils"] = "build/tl-utils/init.lua",
        ["tl-utils.arr"] = "build/tl-utils/arr.lua",
        ["tl-utils.str"] = "build/tl-utils/str.lua",
        ["tl-utils.sh"] = "build/tl-utils/sh.lua",
        ["tl-utils.path"] = "build/tl-utils/path.lua",
        ["tl-utils.fs"] = "build/tl-utils/fs.lua",
        ["tl-utils.tbl"] = "build/tl-utils/tbl.lua",
    }
}
test = {
    type = "command",
    script = "./test/init.lua",
    test_dependencies = {
        "inspect >= 3.1.3"
    },
}
